import React from "react";
import { Route, Routes } from "react-router-dom";
import CarDetailPage from "../pages/CarPage/CarDetailPage";
import SearchCarPage from "../pages/CarPage/SearchCarPage";
import Homepage from "../pages/Homepage/Homepage";
import { Test } from "../pages/TestPage/Test";

export const Routers = () => {
  return (
    <Routes>
      <Route path="/" element={<Homepage />} />
      <Route path="/search" element={<SearchCarPage />} />
      <Route path="/car" element={<CarDetailPage />}>
        <Route path="/car/:id" element={<CarDetailPage />} />
      </Route>
      <Route path="/test" element={<Test />} />
    </Routes>
  );
};
