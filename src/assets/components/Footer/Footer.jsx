import React from "react";
import { Container } from "react-bootstrap";
import { ReactComponent as FacebookIcon } from "../../img/icon_facebook.svg";
import { ReactComponent as InstagramIcon } from "../../img/icon_instagram.svg";
import { ReactComponent as TwitterIcon } from "../../img/icon_twitter.svg";
import { ReactComponent as TwitchIcon } from "../../img/icon_twitch.svg";
import { ReactComponent as EmailIcon } from "../../img/icon_mail.svg";
import "./Footer.scss";

export const Footer = () => {
  return (
    <div className="footer">
      <Container>
        <div className="footer-group-1">
          <span className="item">
            Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000
          </span>
          <span className="item">binarcarrental@gmail.com</span>
          <span className="item">081-233-334-808</span>
        </div>
        <div className="footer-group-2">
          <a href="/" className="item-link">
            Our Services
          </a>
          <a href="/" className="item-link">
            Why Us
          </a>
          <a href="/" className="item-link">
            Testimoni
          </a>
          <a href="/" className="item-link">
            FAQ
          </a>
        </div>
        <div className="footer-group-3">
          <span className="item">Connect with us</span>
          <div className="social-media">
            <a
              href="http://facebook.com"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FacebookIcon />
            </a>
            <a
              href="http://instagram.com"
              target="_blank"
              rel="noopener noreferrer"
            >
              <InstagramIcon />
            </a>
            <a
              href="http://twitter.com"
              target="_blank"
              rel="noopener noreferrer"
            >
              <TwitterIcon />
            </a>
            <a
              href="http://gmail.com"
              target="_blank"
              rel="noopener noreferrer"
            >
              <EmailIcon />
            </a>
            <a
              href="http://twitch.com"
              target="_blank"
              rel="noopener noreferrer"
            >
              <TwitchIcon />
            </a>
          </div>
        </div>
        <div className="footer-group-4">
          <span className="item">Copyright Binar 2022</span>
          <div className="logo"></div>
        </div>
      </Container>
    </div>
  );
};
