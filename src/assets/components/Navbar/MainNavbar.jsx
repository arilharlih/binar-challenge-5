import React from "react";
import { Button, Container, Nav, Navbar } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import "./MainNavbar.scss";

export const MainNavbar = () => {
  const navigate = useNavigate();
  return (
    <Navbar variant="light" expand="lg" fixed="top">
      <Container>
        <Navbar.Brand onClick={() => navigate(`/`)}></Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse
          id="navbarScroll"
          className="justify-content-end align-items-center"
        >
          <Nav className="align-items-center">
            <Nav.Link href="/our-services">Our Services</Nav.Link>
            <Nav.Link href="/why-us">Why Us</Nav.Link>
            <Nav.Link href="/testimonial">Testimonial</Nav.Link>
            <Nav.Link href="/faq">FAQ</Nav.Link>
            <Nav.Link href="/register">
              <Button variant="success">Register</Button>
            </Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};
